# BC-FI ROSTER

- Development merges: dev branch
- Production branch : master

## Workflow:

- keep Trello board up -to-date

- create a feature branch for your development from dev branch

- add ids, classnames, name functions and variables in a meaningful way
- add comments when clarification is needed

- try to be mindful what changes you commit and how you name the commits (use present tense, describe what the commit is about)

- pull dev branch often and merge it into your feature branch to prevent conflicts

- create merge requests often when you get something working

- when doing the merge request resolve possible conflicts (if unresolvable, discuss the issue with team)

- assign a team member to review the merge request (review checklist below)

- when merge request is accepted, feature branch can be merged into dev

## AVOID AT ALL COSTS

working directly with master -> always merge from dev so we will keep dev up to date and avoid major conflicts between master and dev

## PR & review

Merge request can be made when

- no problems with eslint
- there are no conflicts with dev branch
- feature needs to work
- do not merge broken code
- unit tests are passing (optional at first iteration)
- AND the whole app works with the new changes

REMEMBER! Developer is responsible for solving possible conflicts before the PR can be reviewed.

## Review checklist

- check the application in browser / use Postman with API endpoints
 => Pull the branch to your computer and check what's been done
- check the code:  
Is the code readable?
Are variables, functions, components named clearly?  
Is there enough documentation/comments?  
Is there anything you can see right away that it will not work in the future, something needs to be re-thought or you just have questions on the implementation?  
**DO NOT ACCEPT THE MERGE** if it is not up to the standards we agreed as a team. Instead leave comments, request changes and talk with the dev who made the merge request
- When all questions are answered and possible changes made and the code works, you can accept the merge request

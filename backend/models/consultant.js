const mongoose = require('mongoose');

const consultantSchema = new mongoose.Schema({
    consultant: {
        type: String,
        required: true
    },
    status: String,
    roles: String,
    team: String,
    broker: String,
    endCustomer: String,
    until: String,
    maxAllocation: Number,
    contractStatus: String,
    penalty: String,
    accountManager: String,
});

//changes the data from db so that instead of _id, each item has the property id
consultantSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject.__v;
    }
});

module.exports = mongoose.model('Consultant', consultantSchema);
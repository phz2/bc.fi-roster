require('dotenv').config();

const isTestMode = process.env.NODE_ENV === 'test';

const MONGODB_URI = isTestMode ? process.env.TEST_DB_URI : process.env.DB_URI;
const PORT = process.env.PORT || 3001;
const MODE = process.env.NODE_ENV;

console.log('server environment:', MODE);

module.exports = {
    MONGODB_URI,
    PORT,
    MODE
};
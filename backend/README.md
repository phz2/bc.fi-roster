# backend for roster

## structure

- controllers:
  - routes
- models
  - mongoose schemas and models
- utils
  - config variables
- middleware
- app.js
  - application logic, routers etc
- index.js
  - server

## scripts

- npm run dev - starts server in development mode with nodemon
- npm start - starts server in production mode
- npm run test - runs tests with Jest serially displaying individual tests
- npm run eslint - runs eslint - fix possible errors from eslint before PR

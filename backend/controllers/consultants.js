const consultantRouter = require('express').Router();
const Consultant = require('../models/consultant');

consultantRouter.post('/', async (req, res) => {
    const { body } = req;
    const consultant = new Consultant({ ...body });
    await consultant.save();
    res
        .status(201)
        .json(consultant);
});

consultantRouter.get('/', async (req, res) => {
    const consultant = await Consultant.find({});
    res
        .json(consultant);
});

//we query by Id method on our consultant model to find the single data. data is returned in a promise
// if no data found error 404 is thrown

consultantRouter.get('/:id', async (req, res) => {
    const { id } = req.params;
    const consultant = await Consultant.findById(id);
    if (!consultant) {
        return res.status(404).end();
    }
    res
        .json(consultant);
});

//updates only the the changed data
consultantRouter.patch('/:id', async (req, res) => {
    const { id } = req.params;
    const updatedData = req.body;
    const updatedConsultant = await Consultant.findByIdAndUpdate(id, updatedData, { new: true });
    res
        .json(updatedConsultant);
});

//delete
//we pass an id string corresponding to the document we want to delete
consultantRouter.delete('/:id', async (req, res) => {
    const { id } = req.params;
    const consultant = await Consultant.findById(id);
    if (!consultant) {
        res.status(404).end();
    } else {
        await Consultant.findByIdAndRemove(id);
        res.status(204).end();
    }
});



module.exports = consultantRouter;
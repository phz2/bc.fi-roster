const Consultant = require('../models/consultant');

const initialConsultants = [
    {
        consultant: 'John Doe',
        status: 'B',
        roles: 'Tech/Sales',
        team: 'Team Audi',
        broker: '',
        endCustomer: 'Acme.co',
        until: new Date('December 24, 2022'),
        maxAllocation: 100,
        contractStatus: 'pending',
        penalty: 0,
        accountManager: 'Tom Ford'
    },
    {
        consultant: 'Jean Paul',
        status: 'G',
        roles: 'Tech/sales',
        team: 'Team Ferari',
        broker: 'Enerst and young',
        endCustomer: 'Elisa Oy',
        until: new Date('March 15, 2021'),
        maxAllocation: 100,
        contractStatus: 'in progress',
        penalty: 100,
        accountManager: 'Maria'
    },
    {
        consultant: 'Tad Bull',
        status: 'G',
        roles: 'Tech/sales',
        team: 'Team Audi',
        broker: 'audit heads',
        endCustomer: 'DNA Oy',
        until: new Date('September 1, 2025'),
        maxAllocation: 100,
        contractStatus: 'on going',
        penalty: '0',
        accountManager: 'Nina'
    },
];

const createConsultant = async (obj) => {
    const consultant = new Consultant({ ...obj });
    const savedConsultant = await consultant.save();
    return savedConsultant;
};

const getAllConsultants = async () => {
    const consultants = await Consultant.find({});
    return consultants.map(consultant => consultant.toJSON());
};

const nonExistingId = async () => {
    const data = {
        consultant: 'Tobe Deleted',
    };
    const consultant = new Consultant(data);
    await consultant.save();
    await consultant.remove();
    return consultant._id.toString();
};

module.exports = {
    initialConsultants,
    createConsultant,
    getAllConsultants,
    nonExistingId
};
const mongoose = require('mongoose');
const supertest = require('supertest');
const app = require('../app');
const api = supertest(app);
const Consultant = require('../models/consultant');
const helper = require('./testHelper');


describe('consultant api', () => {
    //create a controlled test environment
    beforeEach(async () => {
        await Consultant.deleteMany({});

        for (let consultant of helper.initialConsultants) {
            await helper.createConsultant(consultant);
        }
    });
    afterAll(async () => {
        await mongoose.connection.close();
    });

    describe('GET /api/consultants', () => {
        it('should return initial consultants', async () => {
            const response = await api.get('/api/consultants');
            expect(response.body).toHaveLength(helper.initialConsultants.length);
        });
        it('should return data as json', async () => {
            await api
                .get('/api/consultants')
                .expect(200)
                .expect('Content-Type', /application\/json/);
        });
        it('should return consultants with id', async () => {
            const response = await api.get('/api/consultants');
            const consultants = response.body;

            consultants.forEach(consultant =>
                expect(consultant).toHaveProperty('id')
            );
        });

    });

    describe('GET /api/consultants/:id', () => {
        it('should return a specific consultant with valid id', async () => {
            const consultants = await helper.getAllConsultants();
            const consultantToTest = consultants[0];

            const resultConsultant = await api
                .get(`/api/consultants/${consultantToTest.id}`)
                .expect(200);

            //needed for the data to be in the same format
            const parsedCounsultantToTest = JSON.parse(JSON.stringify(consultantToTest));

            expect(resultConsultant.body).toMatchObject(parsedCounsultantToTest);
        });

        it('should return Bad Request with non-valid id', async () => {
            const nonValidId = 'nonValidId';
            await api
                .get(`/api/consultants/${nonValidId}`)
                .expect(400);
        });

        it('should return Not Found when item does not exist', async () => {
            const nonExistingId = await helper.nonExistingId();

            await api
                .get(`/api/consultants/${nonExistingId}`)
                .expect(404);
        });
    });


    describe('POST /api/consultants', () => {
        it('should create a new consultant', async () => {
            const consultant = {
                consultant: 'Test Consultant',
                status: 'T',
                roles: 'Testing',
                team: 'Test Team',
                broker: 'test heads',
                endCustomer: 'test Oy',
                until: new Date('June 14, 2027'),
                maxAllocation: 100,
                contractStatus: 'on going',
                penalty: '0',
                accountManager: 'Nina'
            };

            await api
                .post('/api/consultants')
                .send(consultant)
                .expect(201)
                .expect('Content-Type', /application\/json/);

            const response = await api.get('/api/consultants');
            expect(response.body).toHaveLength(helper.initialConsultants.length + 1);
        });
        it('should return Bad Request when creating a new consultant without name', async () => {
            const consultant = {
                status: 'T',
                roles: 'Testing',
                team: 'Test Team',
                broker: 'test heads',
            };

            await api
                .post('/api/consultants')
                .send(consultant)
                .expect(400);
        });
    });
    describe('DELETE /api/consultants/:id', () => {
        it('should succesfully delete specified consultant', async () => {
            const consultants = await helper.getAllConsultants();
            const consultantToDelete = consultants[0].id;

            await api
                .delete(`/api/consultants/${consultantToDelete}`)
                .expect(204);

            const consultantsAfterDelete = await helper.getAllConsultants();
            const result = consultantsAfterDelete.find(consultant => consultant.id === consultantToDelete);
            expect(result).toBe(undefined);
        });
        it('shoud return Bad Request when trying to delete item with non-valid id', async () => {
            const nonValidId = 'nonValidId';

            await api
                .delete(`/api/consultants/${nonValidId}`)
                .expect(400);
        });

        it('shoud return Not Found when trying to delete item with non-existing id', async () => {
            const nonExistingId = await helper.nonExistingId();

            await api
                .delete(`/api/consultants/${nonExistingId}`)
                .expect(404);
        });
    });
    describe('PATCH /api/consultants/:id', () => {
        it('should succesfully update the name of specified consultant', async () => {
            const consultants = await helper.getAllConsultants();
            const consultantToUpdate = consultants[1];

            const updateData = {
                consultant: 'Madam Test Consultant'
            };

            const result = await api
                .patch(`/api/consultants/${consultantToUpdate.id}`)
                .send(updateData)
                .expect(200);

            expect(result.body.consultant).toEqual(updateData.consultant);
        });
        it('should not save properties that are not specified in schema', async () => {
            const consultants = await helper.getAllConsultants();
            const consultantToUpdate = consultants[1];

            const updateData = {
                height: '170cm'
            };

            //this should propably return an error/bad request => can I write custom validation to schema for this?
            const result = await api
                .patch(`/api/consultants/${consultantToUpdate.id}`)
                .send(updateData)
                .expect(200);

            expect(result.body).not.toHaveProperty('height');
        });


    });


});




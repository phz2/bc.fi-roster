const config = require('./utils/config');
const express = require('express');
require('express-async-errors');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const consultantRouter = require('./controllers/consultants');
const middleware = require('./middleware/middleware');

const app = express();

mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
    .then(() => {
        console.log('connected to MongoDB');
    })
    .catch((error) => {
        console.log('error connection to MongoDB:', error.message);
    });

if (config.MODE === 'development') {
    app.use(cors());
}
app.use(express.static(path.join(__dirname, '../', 'frontend', 'build')));
app.use(express.json());

//define routers
//test route for making sure server is up and running
app.use('/ping', (req, res) => {
    res.send('pong');
});

app.use('/api/consultants', consultantRouter);

// unknown endpoint
app.use(middleware.unknownEndpoint);

//error handler
app.use(middleware.errorHandler);

module.exports = app;
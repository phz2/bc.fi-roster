# roster frontend

## structure

- components

###
The logic for the Roster table is as follows:
App is the main.
Table is rendered on the App.
Inside the Table you have the Rows (X axis)
and inside each Row there are cells, which form the (Y) axis.


- services
  - api

## scripts

- npm run eslint - runs eslint => fix problems before PR
- npm run cypress:open - opens cypress

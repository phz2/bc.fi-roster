export const sortUntil = (a, b) => {
    const untilA = a.until
        .replace('/', '')
        .replace('/', '')
        .replace(/(\d{2})(\d{2})(\d{4})/, '$3$2$1');
    const untilB = b.until
        .replace('/', '')
        .replace('/', '')
        .replace(/(\d{2})(\d{2})(\d{4})/, '$3$2$1');
    if (untilA < untilB) {
        return -1;
    }
    if (untilA > untilB) {
        return 1;
    }
    return 0;
};

const exists = (value) => value !== undefined && value !== null && value !== '';
const isZero = (value) => value === '0';

export const includesValue = (property, inputValue) => {
    if (exists(property)) {
        if (isZero(inputValue)) {
            return property.toString() === inputValue.toString();
        }
        return property
            .toString()
            .toLowerCase()
            .includes(inputValue.toString().toLowerCase());
    } else {
        return false;
    }
};

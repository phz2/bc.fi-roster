const isDev = window.location.hostname === 'localhost';
export const baseUrl = isDev ? 'http://localhost:3001/api/consultants' : '/api/consultants';
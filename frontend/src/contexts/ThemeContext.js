import React, { createContext, useEffect } from 'react';
import useToggle from '../hooks/useToggle';
import { light, dark } from '../assets/theme';

export const ThemeContext = createContext();

export function ThemeProvider(props) {
    const [isDarkMode, toggleTheme] = useToggle(
        localStorage.getItem('theme') === 'light' ? false : true
    );

    useEffect(() => {
        document
            .getElementsByTagName('HTML')[0]
            .setAttribute('data-theme', localStorage.getItem('theme'));
    }, []);

    const toggleThemeChangeHandler = () => {
        if (isDarkMode === false) {
            window.localStorage.setItem('theme', 'light');
            document
                .getElementsByTagName('HTML')[0]
                .setAttribute('data-theme', localStorage.getItem('theme'));
            toggleTheme(false);
        } else {
            window.localStorage.setItem('theme', 'dark');
            document
                .getElementsByTagName('HTML')[0]
                .setAttribute('data-theme', localStorage.getItem('theme'));
            toggleTheme(true);
        }
    };

    return (
        <ThemeContext.Provider
            value={{
                isDarkMode,
                toggleTheme,
                light,
                dark,
                toggleThemeChangeHandler,
            }}
        >
            {props.children}
        </ThemeContext.Provider>
    );
}

import axios from 'axios';
import { baseUrl } from '../utils/api_config';

export const getConsultants = async () => {
    try {
        const response = await axios.get(baseUrl);
        return response.data;
    } catch (error) {
        console.log(error);
    }
};

export const createConsultant = async (consultantData) => {
    try {
        const response = await axios.post(baseUrl, consultantData);
        console.log('saved to db', response.data);

    } catch (error) {
        console.log('error occured', error);
    }
};

export const deleteConsultant = async (id) => {
    try {
        const response = await axios.delete(`${baseUrl}/${id}`);
        console.log('deleted from db', response.data);
    } catch (error) {
        console.log('error occured', error);
    }
};

export const editConsultant = async (consultantData, id) => {
    try {
        const response = await axios.patch(baseUrl + '/' + id, consultantData);
        console.log('saved to db', response);
    } catch (error) {
        console.log('error occured', error);
    }
};
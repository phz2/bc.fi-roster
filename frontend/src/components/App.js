import React from 'react';
import { ThemeProvider } from '../contexts/ThemeContext';
//components
import SpreadSheet from './Spreadsheet/SpreadSheet';
import ToggleTheme from '../components/ToggleTheme/ToggleTheme';
//styles
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const App = () => {
    return (
        <ThemeProvider>
            <ToggleTheme>
                <CssBaseline />
                <Container maxWidth='xl'>
                    <Typography
                        component='div'
                        style={{ height: '100%', width: '100%' }}
                    >
                        <SpreadSheet />
                    </Typography>
                </Container>
            </ToggleTheme>
        </ThemeProvider>
    );
};

export default App;

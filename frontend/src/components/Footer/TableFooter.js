import React from 'react';

export default function TableFooter() {
    return (
        <div>
            <div className="Row-tfooter">
                <ul className="Footer_legend_contract_type">
                    <p>Legends for Contract Type</p>
                    <li>(*) Final, contract wont be continued.</li>
                    <li>(½) Part Time.</li>
                    <li>(?) Optional contract term extension.</li>
                    <li>(&) Until further notice.</li>
                </ul>
                <ul className="Footer_legend_contract_status">
                    <p>Legends for Contract Status</p>
                    <li>(o) No contract.</li>
                    <li>(u) Old contract, but no update for term continuation.</li>
                    <li>(%) Signed by other party only, do legal check and sign, or ask for new version.</li>
                    <li>(§) Legal Check done - signed by PHZ but not by other party.</li>
                    <li>(€) Signed and filed.</li>
                    <li>(#) FPHZ (legal checked) contract sent to customer but not signed yet by either.</li>
                </ul>
            </div>
        </div>
    );
}

import React from 'react';
import { shallow } from 'enzyme';
import TableFooter from './TableFooter';


describe('Test Footer Component and its content. >>>: ', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<TableFooter />);
        expect(wrapper).toMatchSnapshot();
    });


    test('will be mounted: ', () => {
        expect(wrapper.length).toBe(1);
    });

    test('it should render the contract type legends', () => {
        const contractTypes = wrapper.find('.Footer_legend_contract_type', 'p');
        const typesResult = contractTypes.text();
        expect(typesResult).toBe('Legends for Contract Type(*) Final, contract wont be continued.(½) Part Time.(?) Optional contract term extension.(&) Until further notice.');

    });

    it('it should render the contract status legends', () => {
        const contractStatus = wrapper.find('.Footer_legend_contract_status', 'p');
        const statusResult = contractStatus.text();
        expect(statusResult).toBe('Legends for Contract Status(o) No contract.(u) Old contract, but no update for term continuation.(%) Signed by other party only, do legal check and sign, or ask for new version.(§) Legal Check done - signed by PHZ but not by other party.(€) Signed and filed.(#) FPHZ (legal checked) contract sent to customer but not signed yet by either.');
    });
});

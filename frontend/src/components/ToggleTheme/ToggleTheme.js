import React, { useContext } from 'react';
import { ThemeContext } from '../../contexts/ThemeContext';

const ToggleTheme = (props) => {
    const { dark, light } = useContext(ThemeContext);
    const { isDarkMode } = useContext(ThemeContext);

    const styles = {
        backgroundColor: isDarkMode
            ? dark.backgroundColor
            : light.backgroundColor,
        color: isDarkMode ? dark.color : light.color,
    };
    console.log('THEME MODE: ', isDarkMode);
    return <div style={styles}>{props.children}</div>;
};

export default ToggleTheme;

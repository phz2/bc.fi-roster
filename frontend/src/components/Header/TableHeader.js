import React from 'react';

export default function TableHeader() {
    return (
        <div className='TableHeader'>
            <h1>PHZ Consultant Roster</h1>
        </div>
    );
}

import React from 'react';
import { shallow } from 'enzyme';
import TableHeader from './TableHeader';


describe('Test Header Component and its content. >>>: ', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<TableHeader />);
    });

    it('will be mounted: ', () => {
        expect(wrapper.length).toBe(1);
    });
});

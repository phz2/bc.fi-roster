import React from 'react';
import PropTypes from 'prop-types';

const FilterByName = ({ value, filterHandler }) => {
    return (
        <input
            id='filterByName'
            type='text'
            name='consultant'
            value={value}
            onChange={filterHandler}
            placeholder='Search consultant, status or team'
        />
    );
};

FilterByName.propTypes = {
    filterHandler: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default FilterByName;

import React from 'react';

const FilterTeam = ({ employeeData, filterTeam, value }) => {
    const getTeam = entries => {
        const _result = [];

        entries.forEach(data => {
            if (_result.indexOf(data.team) < 0) {
                _result.push(data.team);
            }
        });

        return _result;
    };

    return (
        <select
            id='teamFilter'
            value={value}
            onChange={filterTeam}
        >
            <option value="" disabled selected hidden>Team</option>
            {getTeam(employeeData).map(team => (
                <option key={team} value={team} >
                    {team}
                </option>
            ))}
        </select>

    );
};

export default FilterTeam;

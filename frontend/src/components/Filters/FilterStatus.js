import React from 'react';
import PropTypes from 'prop-types';

const FilterStatus = ({ value, filterStatus }) => {
    return (
        <input
            id='filterStatus'
            type='text'
            name='status'
            value={value}
            onChange={filterStatus}
            placeholder='Search consultant, status or team'
        />
    );
};

FilterStatus.propTypes = {
    filterStatus: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default FilterStatus;

import React from 'react';
import './searchField.css';

export default function SearchField({ value, searchColumns, placeholder
})
{
    return (
        <input
            className= '__searchfield__'
            type='text'
            value={value}
            placeholder={placeholder}
            onChange={searchColumns}
        />
    );
}


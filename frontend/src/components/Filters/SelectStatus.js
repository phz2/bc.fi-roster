import React from 'react';

const SelectStatus = ({ employeeData, filterStatus, value }) => {
    const getStatus = entries => {
        const _result = [];

        entries.forEach(data => {
            if (_result.indexOf(data.status) < 0) {
                _result.push(data.status);
            }
        });

        return _result;
    };

    return (
        <select
            value={value}
            onChange={filterStatus}
        >
            <option value="" disabled selected hidden>Team</option>
            {getStatus(employeeData).map(status => (
                <option key={status} value={status} >
                    {status}
                </option>
            ))}
        </select>

    );
};

export default SelectStatus;






// import React from 'react';
// import{ ErrorMessage, Field } from 'formik';
// import TextError from './TextError';

// export default function SelectStatus () {
//     //const { options, ...rest } = props;
//     return(
//         <div className>
//             <Field
//                 value= {''}
//                 name='status'
//                 as='select'

//             >


//             </Field>
//             <ErrorMessage name={name} component={TextError}/>


//         </div>
//     );

// }

/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useContext, Fragment } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from '../../contexts/ThemeContext';
//helpers
import { editConsultant } from '../../services/consultant';
import { sortUntil, includesValue } from '../../utils/helperFunctions';
//hooks
import useFilterInput from '../../hooks/useFilterInput';
//components
import Row from './Row';
//styles+icons
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';

const Table = ({ consultants, removeConsultant }) => {
    //data arrays
    const [filteredConsultants, setFilteredConsultants] = useState(consultants);
    // states for editing
    const [isEditing, setIsEditing] = useState(false);
    const [editingCell, setEditingCell] = useState('');
    //filtering
    const consultantByName = useFilterInput('text');
    const status = useFilterInput('text');
    const roles = useFilterInput('text');
    const team = useFilterInput('text');
    const broker = useFilterInput('text');
    const endCustomer = useFilterInput('text');
    const maxAllocation = useFilterInput('text');
    const contractStatus = useFilterInput('text');
    const penalty = useFilterInput('text');
    const accountManager = useFilterInput('text');
    // color theme
    const { isDarkMode } = useContext(ThemeContext);

    useEffect(() => {
        const filteredList =
            consultants &&
            consultants
                .filter(
                    (consultant) =>
                        consultantByName.value === '' ||
                        includesValue(
                            consultant.consultant,
                            consultantByName.value
                        )
                )
                .filter(
                    (consultant) =>
                        status.value === '' ||
                        includesValue(consultant.status, status.value)
                )
                .filter(
                    (consultant) =>
                        roles.value === '' ||
                        includesValue(consultant.roles, roles.value)
                )
                .filter(
                    (consultant) =>
                        team.value === '' ||
                        includesValue(consultant.team, team.value)
                )
                .filter(
                    (consultant) =>
                        broker.value === '' ||
                        includesValue(consultant.broker, broker.value)
                )
                .filter(
                    (consultant) =>
                        endCustomer.value === '' ||
                        includesValue(consultant.endCustomer, endCustomer.value)
                )
                .filter(
                    (consultant) =>
                        maxAllocation.value === '' ||
                        includesValue(
                            consultant.maxAllocation,
                            maxAllocation.value
                        )
                )
                .filter(
                    (consultant) =>
                        contractStatus.value === '' ||
                        includesValue(
                            consultant.contractStatus,
                            contractStatus.value
                        )
                )
                .filter(
                    (consultant) =>
                        penalty.value === '' ||
                        includesValue(consultant.penalty, penalty.value)
                )
                .filter(
                    (consultant) =>
                        accountManager.value === '' ||
                        includesValue(
                            consultant.accountManager,
                            accountManager.value
                        )
                );

        setFilteredConsultants(filteredList);
    }, [
        consultants,
        consultantByName.value,
        status.value,
        roles.value,
        team.value,
        broker.value,
        endCustomer.value,
        maxAllocation.value,
        contractStatus.value,
        penalty.value,
        accountManager.value,
    ]);

    // edit cells
    const selectEditingCell = (editingData, id) => {
        setIsEditing(!isEditing);
        if (isEditing) setEditingCell(editingData);
    };

    const submitUpdatedData = (id, data) => {
        editConsultant(data, id);
        alert('udpated');
    };

    // sort until
    const sortUntilAscHandler = () => {
        const sortedList = [...filteredConsultants].sort(sortUntil);
        setFilteredConsultants(sortedList);
    };

    const sortUntilDescHandler = () => {
        const sortedList = [...filteredConsultants].sort(sortUntil).reverse();
        setFilteredConsultants(sortedList);
    };

    const renderedConsultantlist =
        filteredConsultants &&
        filteredConsultants.map((consultant) => (
            <Row
                key={consultant.id}
                employee={consultant}
                id={consultant.id}
                submitUpdatedData={submitUpdatedData}
                selectEditingCell={selectEditingCell}
                editingCell={editingCell}
                removeConsultant={removeConsultant}
            />
        ));

    const darkModeInputColor = { color: isDarkMode && '#f3f2f1' };

    return (
        <Fragment>
            <div className='Row-thead-container'>
                <div className='Row-thead'>
                    <div className='Row-td'>Consultant</div>
                    <div className='Row-td'>Status</div>
                    <div className='Row-td'>Roles</div>
                    <div className='Row-td'>Team</div>
                    <div className='Row-td'>Broker</div>
                    <div className='Row-td'>End Customer</div>
                    <div className='Row-td'>Until</div>
                    <div className='Row-td'>Max Allocation</div>
                    <div className='Row-td'>Contract Status</div>
                    <div className='Row-td'>Penalty</div>
                    <div className='Row-td'>Account Manager</div>
                </div>
            </div>
            <div className='filter-row'>
                <Input
                    placeholder='Search consultant'
                    style={darkModeInputColor}
                    {...consultantByName}
                />
                <Input
                    placeholder='status'
                    style={darkModeInputColor}
                    {...status}
                />
                <Input
                    placeholder='roles'
                    style={darkModeInputColor}
                    {...roles}
                />
                <Input
                    placeholder='team'
                    style={darkModeInputColor}
                    {...team}
                />
                <Input
                    placeholder='broker'
                    style={darkModeInputColor}
                    {...broker}
                />
                <Input
                    placeholder='end customer'
                    style={darkModeInputColor}
                    {...endCustomer}
                />
                <div className='sort-btn'>
                    <IconButton
                        onClick={sortUntilAscHandler}
                        color='inherit'
                    >
                        <ArrowDownwardIcon fontSize='small' />
                    </IconButton>
                    <IconButton
                        onClick={sortUntilDescHandler}
                        color='inherit'
                    >
                        <ArrowUpwardIcon fontSize='small' />
                    </IconButton>
                </div>
                <Input
                    placeholder='max allocation'
                    style={darkModeInputColor}
                    {...maxAllocation}
                />
                <Input
                    placeholder='contract status'
                    style={darkModeInputColor}
                    {...contractStatus}
                />
                <Input
                    placeholder='penalty'
                    style={darkModeInputColor}
                    {...penalty}
                />
                <Input
                    placeholder='account manager'
                    style={darkModeInputColor}
                    {...accountManager}
                />
            </div>
            <div className='table'>{renderedConsultantlist}</div>
        </Fragment>
    );
};

Table.propTypes = {
    consultants: PropTypes.array.isRequired,
    removeConsultant: PropTypes.func.isRequired,
};

export default Table;

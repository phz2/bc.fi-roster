import React, {
    useCallback,
    useEffect,
    useState,
    useContext,
    Fragment,
} from 'react';
import Switch from 'react-switch';
import { ThemeContext } from '../../contexts/ThemeContext';
//helpers
import {
    createConsultant,
    getConsultants,
    deleteConsultant,
} from '../../services/consultant';
//components
import Table from './Table';
import TableHeader from '../Header/TableHeader';
import TableFooter from '../Footer/TableFooter';
import CreateConsultantForm from '../CreateConsultantForm/CreateConsultantForm';
import Button from '@material-ui/core/Button';
//styles+icons
import { IoMdMoon as Moon, IoMdSunny as Sun } from 'react-icons/io';
import './SpreadSheet.css';

const SpreadSheet = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [consultants, setConsultants] = useState([]);
    const { isDarkMode, toggleThemeChangeHandler } = useContext(ThemeContext);

    const closeModal = () => {
        setIsModalVisible(!isModalVisible);
    };

    const fetchConsultants = async () => {
        const _consultants = await getConsultants();
        setConsultants(_consultants);
    };

    const createNewConsultant = async (data) => {
        const { firstname, lastname, ...rest } = data;
        const newConsultant = {
            consultant: `${firstname} ${lastname}`,
            ...rest,
        };
        await createConsultant(newConsultant);
        setIsModalVisible(false);
        fetchConsultants();
    };

    const removeConsultant = useCallback(async (id, name) => {
        if (
            window.confirm(
                `Are you sure you want to delete consultant ${name}?`
            )
        ) {
            await deleteConsultant(id);
            fetchConsultants();
        }
    }, []);

    useEffect(() => {
        fetchConsultants();
    }, []);

    return (
        <Fragment>
            {isModalVisible ? (
                <CreateConsultantForm
                    closeModal={closeModal}
                    createNewConsultant={createNewConsultant}
                />
            ) : null}
            <div className='container'>
                <div className='Spreadsheet-header-wrapper'>
                    <Switch
                        type='checkbox'
                        checked={isDarkMode}
                        onChange={() => toggleThemeChangeHandler()}
                        className='react-switch'
                        checkedIcon={
                            <Moon
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    height: '100%',
                                    fontSize: 30,
                                    paddingLeft: 0,
                                }}
                                color={isDarkMode && 'yellow'}
                                className='dark'
                            />
                        }
                        uncheckedIcon={
                            <Sun
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    height: '100%',
                                    fontSize: 30,
                                    paddingLeft: 0,
                                }}
                                color={!isDarkMode && 'white'}
                                className='light'
                            />
                        }
                    />
                    <Button
                        className='btn table-header-btn'
                        onClick={() => setIsModalVisible(true)}
                        variant='contained'
                        color='inherit'
                        style={{ backgroundColor: '#ed6930' }}
                    >
                        Add new consultant
                    </Button>
                </div>

                <div className='Table' id='Table'>
                    <div className='Table-container'>
                        <TableHeader />
                        <Table
                            consultants={consultants}
                            removeConsultant={removeConsultant}
                        />
                    </div>
                    <TableFooter />
                </div>
            </div>
        </Fragment>
    );
};

export default SpreadSheet;

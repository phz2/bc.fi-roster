/* eslint-disable no-dupe-keys */
import React, { useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../../contexts/ThemeContext';
import * as Yup from 'yup';
import { Formik, Field, Form } from 'formik';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';

const validationSchema = Yup.object().shape({
    firstname: Yup.string().required('Required').min(3),
    lastname: Yup.string().required('Required').min(3),
    status: Yup.string().oneOf(['B', 'G', 'R'], 'unknown status'),
    roles: Yup.string(),
    status: Yup.string(),
    broker: Yup.string().required('Required').min(3),
    endCustomer: Yup.string(),
    until: Yup.string().matches(),
    maxAllocation: Yup.number().positive().integer().min(0).max(100),
    contractStatus: Yup.string(),
    penalty: Yup.number(),
    accountManager: Yup.string().required('Required').min(3),
    specialPower: Yup.string(),
});

const Row = ({
    employee,
    id,
    submitUpdatedData,
    editingCell,
    selectEditingCell,
    removeConsultant,
}) => {
    const [newInput, setNewInput] = useState(editingCell);
    const [curCell, setCurCell] = useState('');

    const { isDarkMode } = useContext(ThemeContext);

    useEffect(() => {
        if (employee && !editingCell) {
            setNewInput({
                consultant: employee.consultant,
                status: employee.status,
                roles: employee.roles,
                team: employee.team,
                status: employee.status,
                broker: employee.broker,
                endCustomer: employee.endCustomer,
                until: employee.until,
                maxAllocation: employee.maxAllocation,
                contractStatus: employee.contractStatus,
                penalty: employee.penalty,
                accountManager: employee.accountManager,
            });
        }
    }, []);

    const changeHandler = (e) => {
        selectEditingCell(newInput);
        setNewInput({ ...newInput, [e.target.name]: e.target.value });
        setCurCell(e.target.value);
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (curCell === e.target.value)
            setTimeout(() => {
                submitUpdatedData(id, newInput);
                console.log('submitted!', id, newInput);
            }, 2000);
    };

    // save changes when using a tab or moving to other cells
    const keyUpHandler = (e) => {
        e.preventDefault();
        if (e.keyCode === 9 && curCell === e.target.value) {
            setTimeout(() => {
                submitUpdatedData(id, newInput);
                console.log('submitted!', id, newInput);
            }, 2000);
        }
    };

    const darkModeInputColor = {
        backgroundColor: isDarkMode && '#144362',
        color: isDarkMode && '#cdcdcd',
    };

    const selectBgColors =
        newInput.status === 'B'
            ? { backgroundColor: '#40c4ff' }
            : newInput.status === 'G'
                ? { backgroundColor: ' #66bb6a' }
                : newInput.status === 'R'
                    ? { backgroundColor: '#ff5252' }
                    : darkModeInputColor;
    return (
        <div className='edit-row-container' id='EditForm'>
            <Formik
                initialValues={{
                    firstname: '',
                    lastname: '',
                    status: '',
                    roles: '',
                    team: '',
                    broker: '',
                    endCustomer: '',
                    until: '',
                    maxAllocation: 0,
                    contractStatus: '',
                    penalty: 0,
                    accountManager: '',
                }}
                validationSchema={validationSchema}
                onSubmit={async (actions) => {
                    actions.setSubmitting(false);
                    submitHandler();
                    keyUpHandler();
                }}
                validateOnBlur='true'
            >
                {employee && (
                    <Form onKeyUp={keyUpHandler} onBlur={submitHandler}>
                        <div className='edit-row-wrapper'>
                            <Field
                                className='EditForm-td'
                                value={newInput.consultant}
                                onChange={changeHandler}
                                name='consultant'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td-status'
                                value={newInput.status}
                                onChange={changeHandler}
                                name='status'
                                as='select'
                                style={selectBgColors}
                            >
                                <option id='no-status' value='' key=''></option>
                                <option id='status-b' value='B' key='B'>
                                    B
                                </option>
                                <option id='status-g' value='G' key='G'>
                                    G
                                </option>
                                <option id='status-r' value='R' key='R'>
                                    R
                                </option>
                            </Field>

                            <Field
                                className='EditForm-td'
                                value={newInput.roles}
                                onChange={changeHandler}
                                name='roles'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.team}
                                onChange={changeHandler}
                                name='team'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.broker}
                                onChange={changeHandler}
                                name='broker'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.endCustomer}
                                onChange={changeHandler}
                                name='endCustomer'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.until}
                                onChange={changeHandler}
                                name='until'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.maxAllocation}
                                onChange={changeHandler}
                                name='maxAllocation'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.contractStatus}
                                onChange={changeHandler}
                                name='contractStatus'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.penalty}
                                onChange={changeHandler}
                                name='penalty'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Field
                                className='EditForm-td'
                                value={newInput.accountManager}
                                onChange={changeHandler}
                                name='accountManager'
                                type='text'
                                style={darkModeInputColor}
                            />
                            <Button
                                onClick={() =>
                                    removeConsultant(id, employee.consultant)
                                }
                                color='inherit'
                                variant='outlined'
                                size='small'
                                startIcon={
                                    <DeleteIcon
                                        style={{ fontSize: '0.8rem' }}
                                    />
                                }
                                style={{
                                    marginLeft: '0.5rem',
                                    fontSize: '0.7rem',
                                    padding: '0.4rem',
                                    height: '80%',
                                }}
                            >
                                Delete
                            </Button>
                            <button
                                type='submit'
                                style={{ visibility: 'hidden' }}
                            ></button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

Row.propTypes = {
    employee: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.object,
    ]),
};

export default Row;

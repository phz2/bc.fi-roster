import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Form = () => {
    const [formData, setFormData] = useState({
        consultant: '',
        status: '',
        roles: '',
        team: '',
        broker: '',
        endCustomer: '',
        until: '',
        maxAllocation: '',
        contactStatus: '',
        penality: '',
        accountManager: '',
    });

    const changeHandler = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    return (
        <div>
            <input
            />
            <form className='Form' id='Form'>
                <div className='Form-input'>
                    <input
                        name='consultant'
                        value={formData.consultant}
                        onChange={changeHandler}
                        placeholder='consultant'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='status'
                        value={formData.status}
                        onChange={changeHandler}
                        placeholder='placeholder status'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='roles'
                        value={formData.roles}
                        onChange={changeHandler}
                        placeholder='placeholder departments'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='team'
                        value={formData.team}
                        onChange={changeHandler}
                        placeholder='placeholder team'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='broker'
                        value={formData.broker}
                        onChange={changeHandler}
                        placeholder='placeholder broker'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='endCustomer'
                        value={formData.endCustomer}
                        onChange={changeHandler}
                        placeholder='placeholder end customer'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='until'
                        value={formData.until}
                        onChange={changeHandler}
                        placeholder='placeholder until'
                        type='date'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='maxAllocation'
                        value={formData.maxAllocation}
                        onChange={changeHandler}
                        placeholder='placeholder max allocation'
                        type='number'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='contactStatus'
                        value={formData.contactStatus}
                        onChange={changeHandler}
                        placeholder='placeholder contact Status'
                        type='text'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='penalty'
                        value={formData.penalty}
                        onChange={changeHandler}
                        placeholder='placeholder penalty'
                        type='number'
                        min='0.01'
                        step='0.01'
                    />
                </div>
                <div className='Form-input'>
                    <input
                        name='accountManager'
                        value={formData.accountManager}
                        onChange={changeHandler}
                        placeholder='placeholder account Manager'
                        type='text'
                    />
                </div>
            </form>
        </div>
    );
};

Form.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
};

export default Form;

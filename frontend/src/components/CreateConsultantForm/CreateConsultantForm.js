import React, { useContext } from 'react';
import * as Yup from 'yup';
import { ThemeContext } from '../../contexts/ThemeContext';
//components
import { Formik, Field, Form } from 'formik';
import FormOverlay from './FormOverlay';
//styles
import './CreateConsultantForm.css';
import Button from '@material-ui/core/Button';

const validationSchema = Yup.object().shape({
    firstname: Yup.string().required('Required').min(3),
    lastname: Yup.string().required('Required').min(3),
    status: Yup.string(),
    roles: Yup.string(),
    team: Yup.string(),
    broker: Yup.string().required('Required').min(3),
    endCustomer: Yup.string(),
    until: Yup.string().matches(),
    maxAllocation: Yup.number().positive().integer().min(0).max(100),
    contractStatus: Yup.string(),
    penalty: Yup.number(),
    accountManager: Yup.string().required('Required').min(3),
});

const CreateConsultantForm = ({ closeModal, createNewConsultant }) => {
    // color theme
    const { isDarkMode } = useContext(ThemeContext);

    const darkModeInputColor = {
        backgroundColor: isDarkMode && '#144362',
        color: isDarkMode && 'inherit',
    };
    return (
        <>
            <FormOverlay closeModal={closeModal} />
            <div className='form-container' style={darkModeInputColor}>
                <Formik
                    initialValues={{
                        firstname: '',
                        lastname: '',
                        status: '',
                        roles: '',
                        team: '',
                        broker: '',
                        endCustomer: '',
                        until: '',
                        maxAllocation: '',
                        contractStatus: '',
                        penalty: '',
                        accountManager: '',
                    }}
                    validationSchema={validationSchema}
                    onSubmit={async (values, actions) => {
                        actions.setSubmitting(false);
                        actions.resetForm();
                        console.log('form submit', values);
                        createNewConsultant(values);
                    }}
                    validateOnBlur='true'
                >
                    {({ values, touched, errors, handleReset }) => (
                        <Form>
                            <div className='form-wrapper'>
                                <h1 className='form-header'>
                                    Add new consultant:
                                </h1>
                                <div className='form-field-wrap'>
                                    <label htmlFor='firstname'>
                                        First name:
                                    </label>
                                    <Field
                                        type='text'
                                        name='firstname'
                                        placeholder='first name'
                                        value={values.firstname}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.firstname && touched.firstname ? (
                                    <div className='form-error'>
                                        {errors.firstname}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='lastname'>Last name:</label>
                                    <Field
                                        type='text'
                                        name='lastname'
                                        placeholder='last name'
                                        value={values.lastname}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.lastname && touched.lastname ? (
                                    <div className='form-error'>
                                        {errors.lastname}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='status'>Status:</label>
                                    <Field
                                        type='text'
                                        name='status'
                                        placeholder='status'
                                        value={values.status}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.status && touched.status ? (
                                    <div className='form-error'>
                                        {errors.status}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='roles'>Roles:</label>
                                    <Field
                                        type='text'
                                        name='roles'
                                        placeholder='roles'
                                        value={values.roles}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.roles && touched.roles ? (
                                    <div className='form-error'>
                                        {errors.roles}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='team'>Team:</label>
                                    <Field
                                        type='text'
                                        name='team'
                                        placeholder='team'
                                        value={values.team}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.team && touched.team ? (
                                    <div className='form-error'>
                                        {errors.team}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='broker'>Broker:</label>
                                    <Field
                                        type='text'
                                        name='broker'
                                        placeholder='broker'
                                        value={values.broker}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.broker && touched.broker ? (
                                    <div className='form-error'>
                                        {errors.broker}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='endCustomer'>
                                        End Customer:
                                    </label>
                                    <Field
                                        type='text'
                                        name='endCustomer'
                                        placeholder='end customer'
                                        value={values.endCustomer}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.endCustomer && touched.endCustomer ? (
                                    <div className='form-error'>
                                        {errors.endCustomer}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='until'>Until:</label>
                                    <Field
                                        type='text'
                                        name='until'
                                        placeholder='until'
                                        value={values.until}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.until && touched.until ? (
                                    <div className='form-error'>
                                        {errors.until}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='maxAllocation'>
                                        Max Allocation:
                                    </label>
                                    <Field
                                        type='text'
                                        name='maxAllocation'
                                        placeholder='max allocation'
                                        value={values.maxAllocation}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.maxAllocation &&
                                touched.maxAllocation ? (
                                        <div className='form-error'>
                                            {errors.maxAllocation}
                                        </div>
                                    ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='contractStatus'>
                                        Contract Status:
                                    </label>
                                    <Field
                                        type='text'
                                        name='contractStatus'
                                        placeholder='contract status'
                                        value={values.contractStatus}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.contractStatus &&
                                touched.contractStatus ? (
                                        <div className='form-error'>
                                            {errors.contractStatus}
                                        </div>
                                    ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='penalty'>Penalty:</label>
                                    <Field
                                        type='text'
                                        name='penalty'
                                        placeholder='penalty'
                                        value={values.penalty}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.penalty && touched.penalty ? (
                                    <div className='form-error'>
                                        {errors.penalty}
                                    </div>
                                ) : null}
                                <div className='form-field-wrap'>
                                    <label htmlFor='accountManager'>
                                        Account Manager:
                                    </label>
                                    <Field
                                        type='text'
                                        name='accountManager'
                                        placeholder='account manager'
                                        value={values.accountManager}
                                        style={darkModeInputColor}
                                    />
                                </div>
                                {errors.accountManager &&
                                touched.accountManager ? (
                                        <div className='form-error'>
                                            {errors.accountManager}
                                        </div>
                                    ) : null}
                                <div className='button-wrapper-submit'>
                                    <Button
                                        type='submit'
                                        className='btn submit'
                                        color='inherit'
                                        variant='contained'
                                    >
                                        Save Consultant
                                    </Button>
                                </div>
                                <div className='button-wrapper'>
                                    <Button
                                        className='btn'
                                        onClick={handleReset}
                                        color='primary'
                                        variant='contained'
                                    >
                                        Clear fields
                                    </Button>
                                    <Button
                                        className='btn'
                                        onClick={closeModal}
                                        color='primary'
                                        variant='contained'
                                    >
                                        Close without saving
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </>
    );
};

export default CreateConsultantForm;

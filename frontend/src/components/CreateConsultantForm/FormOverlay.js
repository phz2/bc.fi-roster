import React from 'react';

const FormOverlay = ({ closeModal }) => {
    const style = {
        height: '100vh',
        width: '100vw',
        backgroundColor: 'rgba(52, 58, 64, 0.8)',
        position: 'fixed',
        left: '0',
        top: '0',
        display: 'grid',
        justifyItems: 'center',
        alignItems: 'center',
        zIndex: '2000',
    };
    return (
        <div style={style} className='form-overlay' onClick={closeModal}>
        </div>
    );
};

export default FormOverlay;

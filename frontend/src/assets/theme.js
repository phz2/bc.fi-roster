import './css/color.css';

const navy = '#144362';
const lightGrey = '#f3f2f1';
const darkGrey = '#cdcdcd';
// const orange = '#ed6930';

export const light = {
    color: navy,
    backgroundColor: lightGrey,
};

export const dark = {
    color: darkGrey,
    backgroundColor: navy,
};

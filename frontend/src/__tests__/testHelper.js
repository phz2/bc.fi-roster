export const employeesData = [
    {
        consultant: 'John Doe',
        status: 'B',
        roles: 'Tech/Sales',
        team: 'Team Audi',
        broker: '',
        endCustomer: 'Acme.co',
        until: 'Date',
        maxAllocation: 100,
        contractStatus: 'pending',
        penalty: 0,
        accountManager: 'Tom Ford',
    },
    {
        consultant: 'kanye west',
        status: 'G',
        roles: 'Marketing',
        team: 'Team Ferrari',
        broker: '',
        endCustomer: 'Hesburger',
        until: 'Date',
        maxAllocation: 100,
        contractStatus: 'signed',
        penalty: 0,
        accountManager: 'Olli',
    },
];

// has the data changed since our last test?
test('employeeData holds relevant information.', () => {
    expect(employeesData).toMatchSnapshot();
    expect(employeesData).toHaveLength(2);
});

// are all necessary categories available?
for (let i = 0; i < employeesData.length; i += 1) {
    it(`employeesData[${i}] should have properties (consultant, status, roles, team, broker, endCustomer, until, maxAllocation, contractStatus, penalty, accountManager)`, () => {
        expect(employeesData[i]).toHaveProperty('consultant');
        expect(employeesData[i]).toHaveProperty('status');
        expect(employeesData[i]).toHaveProperty('roles');
        expect(employeesData[i]).toHaveProperty('team');
        expect(employeesData[i]).toHaveProperty('broker');
        expect(employeesData[i]).toHaveProperty('endCustomer');
        expect(employeesData[i]).toHaveProperty('until');
        expect(employeesData[i]).toHaveProperty('maxAllocation');
        expect(employeesData[i]).toHaveProperty('contractStatus');
        expect(employeesData[i]).toHaveProperty('penalty');
        expect(employeesData[i]).toHaveProperty('accountManager');
    });
}

// does the data truly match with the data?
test('employee data has Kanye West and matches as an object', () => {
    const testEmployee = {
        consultant: 'kanye west',
        status: 'G',
        roles: 'Marketing',
        team: 'Team Ferrari',
        broker: '',
        endCustomer: 'Hesburger',
        until: 'Date',
        maxAllocation: 100,
        contractStatus: 'signed',
        penalty: 0,
        accountManager: 'Olli',
    };
    expect(employeesData[1]).toMatchObject(testEmployee);
});


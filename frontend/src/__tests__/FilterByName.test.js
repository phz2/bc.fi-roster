import React from 'react';
import { shallow } from 'enzyme';
import FilterByName from '../components/Filters/FilterByName';


describe('FilterByName', () => {
    test('initiates with employeeData', () => {
        const mockHandleChange = jest.fn();
        const placeholderValue = 'Kanye West';
        const wrapper = shallow(<FilterByName value={placeholderValue} onChange={mockHandleChange} />);
        const createInput = wrapper.find('#filterByName');

        expect(createInput.props().value).toBe(placeholderValue);
    });
});
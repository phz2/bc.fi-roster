import React from 'react';
import { shallow } from 'enzyme';
import FilterTeam from '../components/Filters/FilterTeam';


describe('FilterTeam', () => {
    test('initiates with employeeData', () => {
        // the Teamfilter requires the fakeArray.
        const fakeArray = [];
        const mockfunction = jest.fn();
        const placeholderValue = 'Team Audi';
        const wrapper = shallow(<FilterTeam employeeData={fakeArray} value={placeholderValue} onChange={mockfunction} />);
        const createInput = wrapper.find('#teamFilter');

        expect(createInput.props().value).toBe(placeholderValue);
    });
});
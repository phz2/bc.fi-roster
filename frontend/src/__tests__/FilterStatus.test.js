import React from 'react';
import { shallow } from 'enzyme';
import FilterStatus from '../components/Filters/FilterStatus';


describe('FilterStatus', () => {
    test('initiates with employeeData', () => {
        const mockHandleChange = jest.fn();
        const placeholderValue = 'G';
        const wrapper = shallow(<FilterStatus value={placeholderValue} onChange={mockHandleChange} />);
        const createInput = wrapper.find('#filterStatus');

        expect(createInput.props().value).toBe(placeholderValue);
    });
});